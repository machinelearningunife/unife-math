/**
 *  This file is part of BUNDLE.
 * 
 *  BUNDLE is a probabilistic reasoner for OWL 2 ontologies.
 * 
 *  BUNDLE is a module of the LEAP system, but can be used as standalone.
 * 
 *  LEAP was implemented as a plugin of DL-Learner http://dl-learner.org, 
 *  but some components can be used as stand-alone.
 * 
 *  LEAP and all its modules are free software; you can redistribute the and/or modify
 *  them under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  LEAP and all its parts are distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */
package it.unife.ml.math.utilities;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 *
 * @author Giuseppe Cota, Riccardo Zese
 *
 */
public class MathUtilities {
    
    public static double roundWithPow(double num, long pow) {
        return  Math.round(num * pow) / pow;
    }
    
    public static double roundWithAccuracy(double num, int accuracy) {
        long pow = (long) Math.pow(10, accuracy);
        return roundWithPow(num, pow);
    }
    
    public static double roundWithAccuracy(long num, int accuracy) {
        return roundWithAccuracy((double) num, accuracy);
    }
    
    public static double divideRoundWithAccuracy(double dividend, double divisor, int accuracy) {
        return roundWithAccuracy((dividend / divisor), accuracy);
    }
    
    public static double divideRoundWithAccuracy(long dividend, long divisor, int accuracy) {
        return roundWithAccuracy(((double) dividend / divisor), accuracy);
    }
    
    public static double divideRoundWithPow(long dividend, long divisor, long pow) {
        return roundWithPow(((double) dividend / divisor), pow);
    }
    
    public static BigDecimal getBigDecimal(double num, int accuracy) {
        Double roundedNum = roundWithAccuracy(num, accuracy);
        return getBigDecimal(roundedNum.toString(), accuracy);
    }
    
    public static BigDecimal getBigDecimal(String num, int accuracy) {
        return new BigDecimal(num).setScale(accuracy, RoundingMode.HALF_UP);
    }
}
